
class MyClass(GeneratedClass):
    def __init__(self):
        GeneratedClass.__init__(self, False)
        self.motion = ALProxy("ALMotion")
        self.positionErrorThresholdPos = 0.01
        self.positionErrorThresholdAng = 0.03

    def onLoad(self):
        pass

    def onUnload(self):
        self.motion.moveToward(0.0, 0.0, 0.0)

    def onInput_onStart(self):
        import almath
        # The command position estimation will be set to the sensor position
        # when the robot starts moving, so we use sensors first and commands later.
        x = 0.1
        y = 0
        theta = 0
        enableArms = True
        
        initPosition = almath.Pose2D(self.motion.getRobotPosition(True))
        targetDistance = almath.Pose2D(x, y ,theta  )
        expectedEndPosition = initPosition * targetDistance
        self.motion.setMoveArmsEnabled(enableArms, enableArms)
        self.motion.moveTo(x, y, theta * almath.PI / 180)

    def onInput_onStop(self):
        self.onUnload()
