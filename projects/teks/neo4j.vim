neo4j commands:
match (n:NWPCTFAQ)-[r:HAS]-(m:NWPQNA) return labels(n) as NWPCTFAQ, type(r) as HAS,m as NWPQNA shoulda use the count as well

ORDER BY size(keys(n)) DESC
To show the node property:
labels(n), keys(n), size(keys(n)), count(*), you also can hve collect

To show the relation property:
type(r)

To update the node of the neo4j node, use match and set 
MATCH (n { name: 'Andres' })
SET n.surname = 'Taylor'
RETURN n.name, n.surname

to remove a label, se remove
setting parameters:
:params {nodes: [{name: "John", age: 18}, {name: "Phill", age: 23}]}
to show all the params
:params
:params {que:"XXXXXXXXXX"}
match (n:FJTQNA) where $que contains n.keyword2 and $que contains n.keyword1 return n

Usefull cypher commands:
match (n:FJTFAQ) create (m1:FJTQNA {seeded:"1",question:"你好嗎,我正在測試中",keyword1:"公司",keyword2:"測試",keyword3:null,language:"CT",topic:"chitchat"}), (n)-[:HAS]->(m1) return n, m1
match (n:FJTQNA) where $que contains n.keyword1 return n

Using with to define variables
WITH -3 AS a, 4 AS b
RETURN b - a AS result

Use of regular expression in neo4j 
WITH ['mouse', 'chair', 'door', 'house'] AS wordlist
UNWIND wordlist AS word
WITH word
WHERE word =~ '.*ous.*'
RETURN word
https://neo4j.com/docs/developer-manual/current/cypher/syntax/operators/#query-operators-mathematical

To search by ID, need to use the ID function:

MATCH (s)
WHERE ID(s) = 65110
RETURN s

Useage filter feature:
match (n:FJTQNA)  where filter(x IN n.keyword1 WHERE x contains "好") return n
Overpower the rivescript:
To create:
match (n:FJTFAQ) create (m:FJTQNA {topic:"chitchat", lang:"CT", que:"你叫咩名", kw1:["你","大佬"], kw2:"叫",kw3:["什麼","咩野"],kw4:"名",kw5:"",ans:["我叫SAM","我係呀SAM"],seeded:"1",kwno:4}),(n)-[:HAS]->(m) return n,m
To search:
MATCH (n:FJTFAQ)-[:HAS]-(m) where filter(x IN m.kw1 WHERE $que1 contains x) and filter(x IN m.kw2 WHERE $que1 contains x) and filter(x IN m.kw3 WHERE $que1 contains x) and filter(x IN m.kw4 WHERE $que1 contains x) and filter(x IN m.kw5 WHERE $que1 contains x) return m.que,m.ans
MATCH (n { name: 'Andres' })
SET n.surname = 'Taylor'
RETURN n.name, n.surname

Usage case
MATCH (d:Data)
SET 
d.eistag = CASE WHEN d.minTempC < 0 THEN true END,
d.frosttag = CASE WHEN d.maxTempC < 0 THEN true END,
d.heisserTag = CASE WHEN d.maxTempC >= 0 THEN true END67 DETACH DELETE n;
MATCH (n)
RETURN
CASE
WHEN n.eyes = 'blue'
THEN 1
WHEN n.age < 40
THEN 2
ELSE 3 END AS result

MATCH (n)
RETURN n.name,
CASE
WHEN n.age IS NULL THEN -1
ELSE n.age - 10 END AS age_10_years_ago

Defining variable in neo4j
WITH 2 AS number
we can actually compare the number of keywords in an query in order to test the priority

CPU test:
1. 300 nodes test
2. same result test

1. number of nodes
2. number of keywords

execution time
dbms.logs.query.enabled=true

Meeting summary 
===============================================================
1. node synonym:
2. 
3. 

